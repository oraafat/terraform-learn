resource "aws_vpc" "my_vpc" {
    cidr_block = var.vpc_cider_block
    tags = {
      name = "${var.env}-vpc"
    }
  
}
resource "aws_subnet" "my-subnet-1" {
    vpc_id = aws_vpc.my_vpc.id
     cidr_block = var.subnet_cidr_block
     availability_zone = var.availability_zone
    tags = {
      name = "${var.env}-vpc"
    }
}
resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.my_vpc.id
   
}
resource "aws_route_table" "myapp-route-table" {
    vpc_id = aws_vpc.my_vpc.id
    route  {
     cidr_block = "0.0.0.0/0"
     gateway_id = aws_internet_gateway.myapp-igw.id
    }
  
}
resource "aws_route_table_association" "a-rtb-subnet" {

   subnet_id = aws_subnet.my-subnet-1.id
   route_table_id = aws_route_table.myapp-route-table.id


}
resource "aws_security_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.my_vpc.id

    #traffic incoming acess ssh and from brousers
    ingress  {
      cidr_blocks = [ var.my_ip]
      description = "ssh"
      from_port = 22
      to_port = 22
      protocol= "tcp"
    } 

  ingress  {
      cidr_blocks = [ "0.0.0.0/0"]
      description = "nginx"
      from_port = 8080
      to_port = 8080
      protocol= "tcp"
    } 

    egress {
      cidr_blocks = [ "0.0.0.0/0"]
      
      from_port = 0
      to_port = 0
      protocol= "-1"
      prefix_list_ids = []
      }
      tags = {
        "name" = "mySG"
      }
}

data "aws_ami" "my-ami" {
 
  most_recent      = true
 
  owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

 

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
output "aws_ami_id" {
    value = data.aws_ami.my-ami.id
      
    }


resource "aws_key_pair" "myapp-key" {
  key_name   = "myapp-key"
  public_key = file(var.key-pair)
}
resource "aws_instance" "my-ec2" {
    ami = data.aws_ami.my-ami.id
    instance_type = var.instance_type
    
    subnet_id = aws_subnet.my-subnet-1.id
    vpc_security_group_ids = [ aws_security_group.myapp-sg.id ]
    availability_zone = var.availability_zone
    
    associate_public_ip_address = true
    key_name = aws_key_pair.myapp-key.key_name
      tags = {
      name = "${var.env}-vpc"

    }
    user_data = <<EOF
#!/bin/bash
sudo yum update -y && sudo yum install docker -y
sudo systemctl start docker 
sudo usermod -aG docker ec2-user
docker run -p 8080:80 nginx
EOF 
   // user_data = file("./docker-script.sh")

  
}
output "IP" {
    value = aws_instance.my-ec2.public_ip
  
}